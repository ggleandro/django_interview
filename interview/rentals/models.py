from django.db import models
from django.contrib.auth.models import AbstractUser

class Address(models.Model):
    street = models.CharField(max_length=200)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=2)
    zipcode = models.CharField(max_length=10)

# User Classes

class User(AbstractUser):
  
    PROPERTY_OWNER = 100
    TENANT = 200
    
    USER_TYPES = (
        (PROPERTY_OWNER, 'Property Owner'),
        (TENANT, 'Tenant'),
    )

    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    phone = models.CharField(max_length=15)
    user_type = models.IntegerField(choices=USER_TYPES)  


    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)
    
class PropertyOwnerManager(models.Manager):
  
    def get_queryset(self):
        return (super(PropertyOwnerManager, self)
                .get_queryset()
                .filter(user_type=User.PROPERTY_OWNER))
  
    def create(self, **kwargs):
        kwargs.update(dict(user_type=User.PROPERTY_OWNER))
        super(PropertyOwnerManager, self).create(**kwargs)


class TenantManager(models.Manager):
  
    def get_queryset(self):
        return (super(TenantManager, self)
                .get_queryset()
                .filter(user_type=User.TENANT))

    def create(self, **kwargs):
        kwargs.update(dict(user_type=User.TENANT))
        super(TenantManager, self).create(**kwargs)
        

class PropertyOwner(User):
    objects = PropertyOwnerManager()

    class Meta:
        proxy = True


class Tenant(User):
    objects = TenantManager()

    class Meta:
        proxy = True


# Rental Classes

class Schedule(models.Model):
    rent_starts = models.DateTimeField()
    rent_ends = models.DateTimeField()


class RentalProperty(models.Model):
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    schedules = models.ManyToManyField(Schedule, through='PropertySchedule')
    is_active = models.BooleanField()
    owner = models.ForeignKey(PropertyOwner, on_delete=models.DO_NOTHING)


class RentalPrice(models.Model):
    price = models.DecimalField(max_digits=9, decimal_places=2)
    period_starts = models.DateField()
    period_ends = models.DateField()
    property = models.ForeignKey(RentalProperty, on_delete=models.DO_NOTHING)

    def __str__(self):
        return "{} - {:%Y/%m/%d} to {:%Y/%m/%d} - ${:.2f}".format(
            self.property.id, self.period_starts, self.period_ends, self.price)


class PropertySchedule(models.Model):
    schedule = models.ForeignKey(Schedule, on_delete=models.DO_NOTHING)
    rental_property = models.ForeignKey(RentalProperty, on_delete=models.DO_NOTHING)
    tenant = models.ForeignKey(Tenant, on_delete=models.DO_NOTHING)